import org.jetbrains.kotlin.incremental.deleteDirectoryContents

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.android.library)
    alias(libs.plugins.download)
    alias(libs.plugins.dokka)
    alias(libs.plugins.npm.publish)
    alias(libs.plugins.pat.plugin)
}

group = "org.crumbs"
version = libs.versions.crumbs.library.full.get()

pat {
    publishing {
        gitlabProjectId = "20502818"
        repositoryUrl = "https://gitlab.com/eyeo/crumbs/crumbs-sdk"
        baseArtifactId = "crumbs-core"
    }
    buildConfig {
        packageName = "org.crumbs"
        configs {
            create("android") {
                params["apiToken"] = System.getenv("CRUMBS_API_TOKEN")
                params["apiTestToken"] = System.getenv("CRUMBS_API_TEST_TOKEN")
            }
            create("js") {
                params["apiToken"] = System.getenv("CRUMBS_JS_API_TOKEN")
                params["apiTestToken"] = System.getenv("CRUMBS_JS_API_TEST_TOKEN")
            }
        }
    }
}

kotlin {
    android {
        publishLibraryVariants = listOf("debug", "release")
        publishLibraryVariantsGroupedByFlavor = true
        compilations.all {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
    js(IR) {
        useCommonJs()
        moduleName = "crumbs-core"
        browser {
            commonWebpackConfig {
                outputFileName = "crumbs-core.js"
            }
            testTask {
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
        binaries.library()
    }
    sourceSets {
        all {
            languageSettings.apply {
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
                optIn("kotlinx.coroutines.FlowPreview")
                optIn("kotlin.js.ExperimentalJsExport")
            }
        }
        named("commonMain") {
            dependencies {
                implementation(kotlin("stdlib-common"))
                api(libs.privacy.shield.core)
                api(libs.pat.core)
                api(libs.pat.browser)
            }
        }
        named("commonTest") {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        named("jsMain") {
            dependencies {
                implementation(npm("dateformat", "5.0.3"))
                implementation(libs.pat.web.ext)
            }
        }
        named("jsTest") {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        named("androidMain") {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation(libs.gms.ads.identifier)
            }
        }
        named("androidUnitTest") {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
        named("androidInstrumentedTest") {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
    }
}

android {
    compileSdk = libs.versions.android.target.sdk.get().toInt()

    defaultConfig {
        namespace = "org.crumbs"
        minSdk = libs.versions.android.min.sdk.get().toInt()
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    packaging {
        resources.excludes.add("META-INF/LICENSE*")
        resources.excludes.add("META-INF/*.kotlin_module")
        resources.excludes.add("META-INF/AL2.0")
        resources.excludes.add("META-INF/LGPL2.1")
        resources.excludes.add("META-INF/licenses/ASM")
        jniLibs.excludes.add("win32-x86-64/*")
        jniLibs.excludes.add("win32-x86/*")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    dependencies {
        testImplementation(libs.pat.test)
        androidTestImplementation(libs.pat.test)
        androidTestImplementation(libs.pat.webview)
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}


tasks {
    dokkaHtml {
        outputDirectory.set(rootProject.file("docs/docs"))
        doLast {
            rootProject.file("docs/docs/styles/logo-styles.css")
                .writeText(rootProject.file("docs/docs-styles.css").readText())
        }
    }

    val reportsDirectory = "$buildDir/reports/androidTests/connected"
    val deviceScreenShotsFolder = "/sdcard/Android/data/org.crumbs.test/files/Download/screenshots"
    val hostScreenshotsFolder = "$reportsDirectory/screenshots"

    val cleanScreenshotsTask by registering(Exec::class) {
        description = "cleans the screenshots from the device"
        commandLine("adb", "shell", "rm", "-r", deviceScreenShotsFolder)
        isIgnoreExitValue = true
    }

    val pullScreenshotsTask by registering {
        description =
            "copies screenshots from the device to the host"
        outputs.dir(hostScreenshotsFolder)
        doLast {
            val hostScreenshotsDir = File(hostScreenshotsFolder)
            hostScreenshotsDir.mkdirs()
            hostScreenshotsDir.deleteDirectoryContents()

            exec {
                commandLine("adb", "pull", deviceScreenShotsFolder)
                workingDir = File(reportsDirectory)
                isIgnoreExitValue = true
            }

        }
    }

    val embedScreenshotsTask by registering(EmbedScreenshotsTask::class) {
        description = "embeds failed tests screenshots in the JUnit HTML report"
        onlyIf { getByName("connectedDebugAndroidTest").state.failure != null }
        dependsOn(pullScreenshotsTask)
        inputDirectory = File(hostScreenshotsFolder)
    }

    afterEvaluate {
        tasks.getByName("connectedDebugAndroidTest") {
            dependsOn(cleanScreenshotsTask)
            finalizedBy(embedScreenshotsTask)
        }
    }
}


