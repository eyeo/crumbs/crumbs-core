package org.crumbs.provider

import com.eyeo.pat.PatLogger
import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.UpdatedStoredValue
import com.eyeo.pat.utils.TimeProvider
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.http.isSuccess
import kotlinx.coroutines.withContext
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import org.crumbs.models.CrumbsDomainData
import org.crumbs.models.CrumbsInterest
import kotlin.coroutines.CoroutineContext
import kotlin.math.min
import kotlin.time.Duration.Companion.days

class MetaDataProvider(
    private val coroutineContext: CoroutineContext,
    private val httpClientProvider: HttpClientProvider,
    storageProvider: StorageProvider,
    timeProvider: TimeProvider
) {

    private val interests = UpdatedStoredValue(
        key = INTERESTS_KEY,
        storageProvider = storageProvider,
        timeProvider = timeProvider,
        serializer = ListSerializer(CrumbsInterest.serializer()),
        defaultBuilder = { listOf() },
        updateDelayMs = UPDATE_INTERESTS_INTERVAL_MS
    )

    private val domains = UpdatedStoredValue(
        key = DOMAINS_KEY,
        storageProvider = storageProvider,
        timeProvider = timeProvider,
        serializer = MapSerializer(String.serializer(), CrumbsDomainData.serializer()),
        defaultBuilder = { mapOf() },
        updateDelayMs = UPDATE_DOMAINS_INTERVAL_MS
    )

    internal suspend fun runCron(): Long {
        val interestsUpdate = interests.update()
        {
            withContext(coroutineContext) {
                try {
                    val interestsList = downloadMetaData<List<CrumbsInterest>>(INTERESTS_URL)
                    interests.store(interestsList)
                } catch (e: Exception) {
                    PatLogger.w(TAG, "Failed updating interests", e)
                }

            }
        }

        val domainsUpdate = domains.update()
        {
            withContext(coroutineContext) {
                try {
                    val domainsMap = downloadMetaData<Map<String, CrumbsDomainData>>(DOMAINS_URL)
                    domains.store(domainsMap)
                } catch (e: Exception) {
                    PatLogger.w(TAG, "Failed updating domains", e)
                }
            }
        }

        return min(domainsUpdate, interestsUpdate)
    }

    private suspend inline fun <reified T> downloadMetaData(url: String): T {
        val request = httpClientProvider.client.get(url)
        if (request.status.isSuccess()) {
            return request.body()
        }
        throw Exception("GET $url status: ${request.status}")
    }

    fun getDomainData(url: String): CrumbsDomainData? {
        val hostname = url.getUrlHost()?.replace(Regex("^www\\."), "")
        return getDomains()[hostname]
    }

    fun getInterest(catId: Int): CrumbsInterest? {
        return getInterests().find { it.id == catId }
    }

    private fun getDomains(): Map<String, CrumbsDomainData> {
        return domains.value
    }

    fun getInterests(): List<CrumbsInterest> {
        return interests.value
    }

    companion object {
        private const val TAG = "MetaDataProvider"

        private val UPDATE_INTERESTS_INTERVAL_MS = 30.days.inWholeMilliseconds
        private const val INTERESTS_KEY = "meta_interests"
        private const val INTERESTS_URL =
            "https://easylist-downloads.adblockplus.org/meta-interests.json"

        private val UPDATE_DOMAINS_INTERVAL_MS = 7.days.inWholeMilliseconds
        private const val DOMAINS_KEY = "meta_domains"
        private const val DOMAINS_URL =
            "https://easylist-downloads.adblockplus.org/meta-domains.json"
    }
}