package org.crumbs.migration

import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.privacy.TncPreference
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.PrivacyShieldStatsData
import com.russhwolf.settings.Settings

internal class PreferencesMigration {
    companion object {
        internal const val GLOBAL_STATS_KEY = "ps_global_stats"
        internal const val LEGACY_GLOBAL_STATS_KEY = "crumbs_global_stats"
        internal const val PRIVACY_SHIELD_SETTINGS_KEY = "ps_privacy_settings"

        fun migratePreferences(
            crumbsSettings: Settings,
            privacyShieldSettings: Settings
        ) {

            if (!crumbsSettings.keys.contains(LegacyPrivacySettings.SETTINGS_KEY)) {
                return
            }

            val crumbsStorageProvider = StorageProvider(crumbsSettings)
            val legacySettings = SettingsProvider(
                LegacyPrivacySettings.SETTINGS_KEY,
                crumbsStorageProvider,
                LegacyPrivacySettings.serializer(),
                defaultBuilder = { LegacyPrivacySettings() }
            ).get()

            val privacyShieldStorageProvider = StorageProvider(privacyShieldSettings)
            val privacyShieldSettingsProvider = SettingsProvider(
                PRIVACY_SHIELD_SETTINGS_KEY,
                privacyShieldStorageProvider,
                PrivacySettings.serializer(),
                defaultBuilder = { PrivacySettings() }
            )

            privacyShieldSettingsProvider.edit {
                it.copy(
                    enabled = legacySettings.enabled,
                    hideReferrerHeader = legacySettings.hideReferrerHeader,
                    hideCookieConsentPopups = legacySettings.hideCookieConsentPopups,
                    enableDoNotTrack = legacySettings.enableDoNotTrack,
                    enableGPC = legacySettings.enableGPC,
                    enableCNameCloakingProtection = legacySettings.enableCNameCloakingProtection,
                    removeMarketingTrackingParameters = legacySettings.removeMarketingTrackingParameters,
                    cookiesFilterLevel = legacySettings.cookiesFilterLevel,
                    blockTrackers = legacySettings.enabled,
                    blockThirdPartyCookies = legacySettings.blockThirdPartyCookies,
                    blockHyperlinkAuditing = legacySettings.blockHyperlinkAuditing,
                    enableFingerprintShield = legacySettings.enableFingerprintShield,
                    blockSocialMediaIconsTracking = legacySettings.blockSocialMediaIconsTracking,
                    deAMP = legacySettings.deAMP,
                )
            }

            val legacyStats = crumbsStorageProvider.load(LEGACY_GLOBAL_STATS_KEY, PrivacyShieldStatsData.serializer())
            legacyStats?.let {
                privacyShieldStorageProvider.save(GLOBAL_STATS_KEY, it, PrivacyShieldStatsData.serializer())
            }

            val legacyTncPreference =
                LegacyTncPreference(crumbsStorageProvider)

            TncPreference(privacyShieldStorageProvider).setTermsAndConditionsState(
                legacyTncPreference.getTermsAndConditionsState()
            )

            crumbsSettings.apply {
                remove(LegacyPrivacySettings.SETTINGS_KEY)
                remove(LEGACY_GLOBAL_STATS_KEY)
                remove(LegacyTncPreference.TERMS_AND_CONDITIONS_KEY)
                remove(LegacyTncPreference.TERMS_AND_CONDITION_KEY_VERSION)
            }
        }
    }
}