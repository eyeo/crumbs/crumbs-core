package org.crumbs.migration

import com.eyeo.pat.models.FeatureSettings
import com.eyeo.privacy.models.FilterLevel
import kotlinx.serialization.Serializable
import org.crumbs.CrumbsCore
import kotlin.js.JsExport

@JsExport
@Serializable
data class LegacyPrivacySettings(
    override val enabled: Boolean = false,
    val hideReferrerHeader: Boolean = true,
    val hideCookieConsentPopups: Boolean = false,
    val enableDoNotTrack: Boolean = true,
    val enableGPC: Boolean = true,
    val enableCNameCloakingProtection: Boolean = true,
    val proxyAdvertisingRequests: Boolean = true,
    val removeMarketingTrackingParameters: Boolean = true,
    val cookiesFilterLevel: FilterLevel = FilterLevel.SANDBOX,
    val blockThirdPartyCookies: Boolean = true,
    val blockHyperlinkAuditing: Boolean = true,
    val enableFingerprintShield: Boolean = true,
    val blockSocialMediaIconsTracking: Boolean = true,
    val deAMP: Boolean = false,
    val adsProxyUrl: String = DEFAULT_PROXY,
    val adsProxyAutoConfigUrl: String = PROXY_AUTO_CONFIG_URL,
    /**
     * Domains where privacy protection is disabled
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val allowedDomains: Set<String> = setOf(),
    /**
     * Domains where privacy protection is disabled for this session
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val allowedDomainsThisSession: Set<String> = setOf(),
) : FeatureSettings {
    companion object {
        const val SETTINGS_KEY = "crumbs_privacy_settings"
    }
}

private const val PROXY_LIST_KEY = "proxy_list_key"
private const val PROXY_AUTO_CONFIG_URL = "${CrumbsCore.CRUMBS_WEBSITE}/proxy.pac"
private const val PROXY_LIST_URL = "${CrumbsCore.CRUMBS_WEBSITE}/proxy-domains.json"
private const val PROXY_HOST = "proxyout-crumbs-org-1.uplink.eyeo.it"
private const val PROXY_PORT = 1080
private const val DEFAULT_PROXY = "socks5://$PROXY_HOST:$PROXY_PORT"