package org.crumbs.models

import kotlinx.serialization.Serializable
import org.crumbs.service.ProfileService.Companion.INTEREST_RELEVANT_SCORE
import kotlin.js.JsExport
import kotlin.jvm.JvmOverloads

@JsExport
data class ProfileInterest(
    val interest: CrumbsInterest,
    val enabled: Boolean,
    val shareable: Boolean = false,
    val score: Float = 0f
) {
    val id: Int
        get() = interest.id

    val hasEnoughData
        get() = score > INTEREST_RELEVANT_SCORE
}

val DEVICE_INTEREST =
    CrumbsInterest(-2, "Device", null, null, "\uD83D\uDC64")

val LOCATION_INTEREST =
    CrumbsInterest(DEVICE_INTEREST.id - 1, "Location", null, null, "\uD83D\uDCCD")


internal val EXTRA_INTERESTS =
    arrayListOf(
        DEVICE_INTEREST,
        LOCATION_INTEREST
    )

/**
 * The metadata and interests shared with the partners
 */
@JsExport
@Serializable
data class ShareableProfile @JvmOverloads constructor(
    val country: String? = null,
    @Suppress("NON_EXPORTABLE_TYPE") val interests: List<Int> = emptyList()
)

fun ProfileDeviceInfo.toInterest(): CrumbsInterest = DEVICE_INTEREST.copy(value = "$os, $platform")

@Serializable
internal data class ProfileData(
    var location: ProfileLocation? = null,
    var device: ProfileDeviceInfo? = null,
    var acceptLanguage: String? = null,
    var userAgent: String? = null,
    var lmt: Boolean = false
) {

    fun toInterests(): List<CrumbsInterest> {
        return arrayListOf(
            location?.toInterest(),
            // we don't share device interest for now
            // device.toInterest(),
        ).filterNotNull().toList()
    }

    companion object {
        internal val EXTRA_INTERESTS =
            arrayListOf(
                DEVICE_INTEREST,
                LOCATION_INTEREST
            )
    }
}

internal interface InterestSource {
    fun toInterest(): CrumbsInterest
}

@Serializable
internal data class ProfileLocation(val countryName: String, val countryCode: String) :
    InterestSource {

    override fun toInterest(): CrumbsInterest = LOCATION_INTEREST.copy(value = toString())

    override fun toString() = countryName

    companion object {
        internal val LOCATION_INTEREST =
            CrumbsInterest(DEVICE_INTEREST.id - 1, "Location", null, null, "\uD83D\uDCCD")
    }
}