package org.crumbs.service

import com.eyeo.pat.utils.UrlExtension.toUrl
import io.ktor.http.*
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import org.crumbs.models.ProfileData
import org.crumbs.models.ShareableProfile
import org.crumbs.provider.MetaDataProvider
import org.crumbs.provider.ProfileDataProvider
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals

internal class ProfileServiceTests {

    @RelaxedMockK
    private lateinit var interestsService: InterestsService

    @RelaxedMockK
    private lateinit var safeToShareService: SafeToShareService

    @RelaxedMockK
    private lateinit var profileDataProvider: ProfileDataProvider

    @RelaxedMockK
    private lateinit var dataProvider: MetaDataProvider

    private lateinit var profileService: ProfileService

    private val getMainDomainMock = mockk<(Url) -> String>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        every { getMainDomainMock.invoke(any()) }.answers { (it.invocation.args[0] as Url).host }
        every { profileDataProvider.get() }.returns(ProfileData())

        profileService = ProfileService(
            interestsService,
            safeToShareService,
            getMainDomainMock,
            profileDataProvider,
            dataProvider
        )
    }

    @ExperimentalStdlibApi
    @Test
    fun testShareableProfile() {

        val shareableProfile1 = ShareableProfile(country = "country1")
        val shareableProfile2 = ShareableProfile(country = "country2")
        profileService.setFakeShareableProfile(shareableProfile1)

        every { interestsService.getSettings().isSharingEnabledWith(any()) } returns true

        assertEquals(
            profileService.getShareableProfile(
                "0",
                "https://domain1.fr".toUrl()!!,
                false
            )?.country, shareableProfile1.country
        )
        assertEquals(
            profileService.getShareableProfile(
                "1",
                "https://domain1.fr/otherPath".toUrl()!!,
                false
            )?.country, shareableProfile1.country
        )
        profileService.setFakeShareableProfile(shareableProfile2)
        assertEquals(
            profileService.getShareableProfile(
                "1",
                "https://domain1.fr/path".toUrl()!!,
                false
            )?.country, shareableProfile1.country
        )
        assertEquals(
            profileService.getShareableProfile(
                "0",
                "https://domain2.fr".toUrl()!!,
                false
            )?.country, shareableProfile2.country
        )

        every { interestsService.getSettings().isSharingEnabledWith(any()) } returns false
        assertEquals(
            profileService.getShareableProfile(
                "0",
                "https://domain2.fr".toUrl()!!,
                false
            ), null
        )
    }
}