import com.eyeo.pat.provider.JsResourcesProvider
import com.russhwolf.settings.Settings
import org.crumbs.CrumbsCore
import org.crumbs.CrumbsOptions
import org.crumbs.migration.PreferencesMigration
import org.crumbs.models.AppInfo
import org.crumbs.provider.JsBrowserInfoProvider

@JsExport
object CrumbsJs {
    fun get() = CrumbsCore.get()

    fun setup(
        appName: String,
        appVersion: String,
        crumbsOptions: CrumbsOptions,
        @Suppress("NON_EXPORTABLE_TYPE") crumbsSettings: Settings,
        @Suppress("NON_EXPORTABLE_TYPE") privacyShieldSettings: Settings
    ) {
        // prevent crash in ktor with condition (window === undefined)
        js("globalThis.window = undefined")

        PreferencesMigration.migratePreferences(crumbsSettings, privacyShieldSettings)

        PrivacyShieldJs.setup(crumbsOptions.privacyShieldOptions, privacyShieldSettings)
        CrumbsCore.createInstance(
            PrivacyShieldJs.get(),
            crumbsSettings,
            JsBrowserInfoProvider(AppInfo(name = appName, version = appVersion)),
            JsResourcesProvider(),
            crumbsOptions
        )
    }
}