package org.crumbs

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertTrue
@RunWith(AndroidJUnit4::class)
class CrumbsAndroidTest {

    @Before
    fun init() {
        CrumbsAndroid.release()
    }

    @After
    fun release() {
        CrumbsAndroid.release()
    }

    @Test
    fun testSetup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        //tag::setup[]
        CrumbsAndroid.setup(context)
        //end::setup[]
        //tag::isInitialized[]
        val initialized = CrumbsAndroid.isInitialized()
        //end::isInitialized[]
        assertTrue(initialized)
        //tag::instance[]
        val crumbs = CrumbsAndroid.get()
        //end::instance[]
        assertNotNull(crumbs)
    }
}