package org.crumbs.ui

import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.test.CaptureView.captureFragment
import com.eyeo.pat.test.CaptureView.captureView
import com.eyeo.pat.test.CaptureView.toDp
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.models.*
import org.crumbs.ui.utils.mockInterests
import org.crumbs.ui.view.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class ScreenShotsTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    private val themeId = com.eyeo.privacy.ui.R.style.Theme_PrivacyShield_Settings
    private val activityContainerId = R.id.crumbs_activity_container
    private val cardViewStyle = com.google.android.material.R.attr.materialCardViewElevatedStyle
    private val viewWidth = 340.toDp()

    @Before
    fun setup() {
        CrumbsAndroid.release()
        CrumbsAndroid.setup(context)
        CrumbsAndroid.get().privacy().editSettings().enable(true).apply()
        CrumbsAndroid.get().interests().editSettings().enable(true).apply()
    }

    @After
    fun release() {
        CrumbsAndroid.release()
        CrumbsProvider.testInstance = null
    }

    @Test
    fun captureInterestsFragment() {
        mockInterests()
        captureFragment<CrumbsInterestsFragment>("interests_fragment", themeId, activityContainerId)
    }

    @Test
    fun captureInterestsBoardView() {
        captureView(context, "interests_board_view") {
            CrumbsInterestsBoardView(it).apply {
                setInterests("https://crumbs.org", emptyList())
            }
        }

        captureView(context, "interests_board_view_share") {
            CrumbsInterestsBoardView(it).let { interestsBoardView ->
                val categories = listOf(
                    CrumbsInterest(-1, "English", "en-US", null, "\uD83C\uDF10"),
                    CrumbsInterest(-2, "Device", "Android 11, mobile", null, "\uD83D\uDC64"),
                    CrumbsInterest(-3, "Location", "USA", null, "\uD83D\uDCCD"),
                    CrumbsInterest(23, "Travel", null, null, "\uD83D\uDDFA"),
                    CrumbsInterest(22, "Technology and Computing", null, null, "\uD83D\uDCBB"),
                )
                val domain = "https://crumbs.org"
                //tag::CrumbsInterestsBoardView[]
                interestsBoardView.setInterests(domain, categories)
                //end::CrumbsInterestsBoardView[]
                interestsBoardView
            }
        }
    }
    private fun captureView(
        context: Context,
        name: String,
        width: Int = viewWidth,
        builder: (AppCompatActivity) -> View
    ) {
        captureView(
            context = context,
            themeResId = themeId,
            activityId = activityContainerId,
            cardViewStyle = cardViewStyle,
            name = name,
            width = width,
            builder = builder
        )
    }
}