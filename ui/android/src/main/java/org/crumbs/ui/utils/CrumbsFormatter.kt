package org.crumbs.ui.utils

import android.content.Context
import android.content.res.Resources
import android.content.res.XmlResourceParser
import androidx.annotation.XmlRes
import org.crumbs.models.CrumbsInterest
import org.crumbs.ui.R
import kotlin.collections.set

class CrumbsFormatter private constructor(context: Context) {

    private val interestTranslations = context.resources.getMap(R.xml.interests)

    fun getInterestDisplayName(interest: CrumbsInterest): String {
        val name = "${interest.emoji} ${interestTranslations[interest.id] ?: interest.name}"
        return interest.value?.let { "$name (${interest.value})" } ?: name
    }

    companion object {
        @Volatile
        private lateinit var instance: CrumbsFormatter

        fun getInstance(context: Context): CrumbsFormatter {
            synchronized(this) {
                if (!::instance.isInitialized) {
                    instance = CrumbsFormatter(context)
                }
                return instance
            }
        }

        private const val XML_TAG_ENTRY = "string"
    }

    private fun Resources.getMap(@XmlRes resourceId: Int): Map<Int, String> {
        val defaultsMap: MutableMap<Int, String> = hashMapOf()
        val xmlParser: XmlResourceParser = getXml(resourceId)
        var curTag: String? = null
        var value: String? = null
        var key: Int = Int.MIN_VALUE
        var eventType = xmlParser.eventType
        while (eventType != XmlResourceParser.END_DOCUMENT) {
            if (eventType == XmlResourceParser.START_TAG) {
                curTag = xmlParser.name
                key = xmlParser.getAttributeIntValue(0, Int.MIN_VALUE)
            } else if (eventType == XmlResourceParser.END_TAG) {
                if (xmlParser.name == XML_TAG_ENTRY) {
                    if (key != Int.MIN_VALUE && value != null) {
                        defaultsMap[key] = value
                    }
                    key = Int.MIN_VALUE
                    value = null
                }
                curTag = null
            } else if (eventType == XmlResourceParser.TEXT) {
                if (curTag != null) {
                    value = xmlParser.text.replace("\\'", "'")
                    if (value.firstOrNull() == '"' && value.lastOrNull() == '"') {
                        value = value.substring(1, value.length - 2)
                    }
                }
            }
            eventType = xmlParser.next()
        }
        return defaultsMap
    }

}
