package org.crumbs.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.eyeo.pat.PatLogger
import com.eyeo.pat.utils.CompatExtension.parcelable
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.ui.PrivacyShieldSettingsFragment
import com.eyeo.privacy.ui.analytics.UiEvent.Settings
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.ui.databinding.CrumbsActivityBinding

class CrumbsSettingsActivity : AppCompatActivity(), CrumbsProvider {

    private lateinit var binding: CrumbsActivityBinding
    private lateinit var toolbar: ActionBar
    private var fragmentId = MAIN_FEATURE_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        if (intent.hasExtra(ARGS_THEME)) {
            setTheme(intent.getIntExtra(ARGS_THEME, 0))
        }
        super.onCreate(savedInstanceState)

        if (!CrumbsAndroid.isInitialized()) {
            finish()
            PatLogger.w(TAG, "Crumbs is not initialized. CrumbsActivity can't be open")
            return
        }

        binding = CrumbsActivityBinding.inflate(layoutInflater)
        toolbar = supportActionBar!!
        toolbar.setDisplayHomeAsUpEnabled(parentActivityIntent != null)

        val args = Bundle()
        intent.getStringExtra(ARGS_HIGHLIGHTED_FEATURE)?.let {
            args.putString(ARGS_HIGHLIGHTED_FEATURE, it)
        }
        intent.getStringArrayExtra(ARGS_HIDDEN_FEATURES)?.let {
            args.putStringArray(ARGS_HIDDEN_FEATURES, it)
        }

        binding.tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                fragmentId = if (tab?.position == 0) MAIN_FEATURE_ID else INTERESTS_FEATURE_ID
                replaceFragment(fragmentId, args)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        savedInstanceState?.let {
            fragmentId = it.getInt(ARGS_FRAGMENT, MAIN_FEATURE_ID)
        }

        if (savedInstanceState == null) {
            crumbs?.analytics()?.notifyEvent(Event.Settings.OPEN_SCREEN)
            fragmentId = intent.getIntExtra(ARGS_FRAGMENT, MAIN_FEATURE_ID)
            replaceFragment(fragmentId, args)
        } else {
            binding.tabLayout.selectTab(binding.tabLayout.getTabAt(toFeatureIndex(fragmentId)))
        }
        setContentView(binding.root)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(ARGS_FRAGMENT, fragmentId)
        super.onSaveInstanceState(outState)

    }

    private fun replaceFragment(fragmentId: Int, args: Bundle? = null) {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            when (fragmentId) {
                MAIN_FEATURE_ID ->
                    replace<PrivacyShieldSettingsFragment>(
                        binding.fragmentContainerView.id,
                        args = args
                    )

                INTERESTS_FEATURE_ID ->
                    replace<CrumbsInterestsFragment>(binding.fragmentContainerView.id)
            }
        }
    }

    override fun getParentActivityIntent(): Intent? {
        if (intent.hasExtra(ARGS_PARENT)) {
            return intent.parcelable(ARGS_PARENT)
        }
        return super.getParentActivityIntent()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return false
    }

    companion object {

        private const val TAG = "CrumbsActivity"
        internal const val ARGS_HIGHLIGHTED_FEATURE = "highlighted_feature"
        internal const val ARGS_HIDDEN_FEATURES = "hidden_features"
        private const val ARGS_FRAGMENT = "fragment_id"
        private const val ARGS_PARENT = "parent_intent"
        private const val ARGS_THEME = "theme"

        @JvmStatic
        val MAIN_FEATURE_ID = R.id.crumbs_privacy

        @JvmStatic
        val INTERESTS_FEATURE_ID = R.id.crumbs_interests

        private fun toFeatureIndex(featureId: Int): Int {
            return when (featureId) {
                MAIN_FEATURE_ID -> 0
                INTERESTS_FEATURE_ID -> 1
                else -> Int.MAX_VALUE
            }
        }

        @JvmStatic
        @JvmOverloads
        fun createIntent(
            context: Context,
            selectedFeature: Int = 0,
            parentIntent: Intent? = null,
            highlightedFeature: String? = null,
            hiddenFeatures: Array<String>? = null,
            theme: Int? = null
        ) =
            Intent(context, CrumbsSettingsActivity::class.java).apply {
                if (selectedFeature != 0) {
                    putExtra(ARGS_FRAGMENT, selectedFeature)
                }
                parentIntent?.let {
                    putExtra(ARGS_PARENT, parentIntent)
                }
                highlightedFeature?.let {
                    putExtra(ARGS_HIGHLIGHTED_FEATURE, highlightedFeature)
                }
                hiddenFeatures?.let {
                    putExtra(ARGS_HIDDEN_FEATURES, hiddenFeatures)
                }
                theme?.let {
                    putExtra(ARGS_THEME, it)
                }
            }
    }
}